<?php
function coletarTips(){
$link = mysqli_connect("localhost","root","","semprini");
if(!$link){
    echo "Erro de conexão com o banco";
    die;
}

$listaTips = array();
$listaIds = array();
$id;
$sql = "SELECT * FROM tips WHERE `status` = false";
$resultados = $link->query($sql);

if($resultados->num_rows > 0){
    while ($row = $resultados->fetch_assoc()) {
        array_push($listaIds,$row['id']);
        array_push($listaTips,$row);
    }
    $ids = implode(",",$listaIds); 

    $sqlUpdate = "UPDATE tips SET `status` = true WHERE id IN ($ids)";
    $link->query($sqlUpdate);
   
}

echo json_encode($listaTips);
}
return coletarTips();