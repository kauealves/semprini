function coletarTips() {
    $.ajax({
        url: "coletarTips.php",
        data: listaTips,
        datatype: 'json',
        type: "POST",
        success: function (data) {
            listaTips = data;
        }
    });
    return listaTips;
}

function changeTips(novatip) {

    setTimeout(function () {

        if (novatip['quantidade'] > 99) {
            $("#tipHeader").fadeOut('slow', function () {
                headerTip(novatip);
            });

            $("#tipHeader").fadeIn("slow", function () {
                setTimeout(function () {
                    $("#tipHeader").fadeOut('slow');
                }, 3000)
            });
        }

        $("#tipFooter").fadeOut('slow', function () {
            footerTip(novatip);
        });

        $("#tipFooter").fadeIn("slow", function () {
            setTimeout(function () {
                $("#tipFooter").fadeOut('slow');
            }, 3000)
        });



    }, 2000);
}

function headerTip(novatip) {

    var valorTip = novatip['quantidade'];

    if (valorTip > 99 && valorTip < 1000 && valorTip != 666) {
        $("#tipHeader").html(
            "<p>" + novatip['nome'] + " tipped " + novatip['quantidade'] + " tokens!</p>"
        );
    } else if (valorTip == 666) {
        var audio = new Audio('sounds/chewbacca.swf.mp3');
        audio.play();

    } else if (valorTip > 999) {

        $("#tipHeader").html(
            "<img src='gifs/giphy.gif'>"
        );
    }

}

function footerTip(novatip) {
    $("#tipFooter").html(
        "<p>+" + novatip['quantidade'] + "</p>"
    );
}


function start() {
    var cont = 1;
    var listaTips;
    setInterval(function () {
        tips = jQuery.parseJSON(coletarTips());
        tips.forEach(tip => {
            console.log(tip);
            changeTips(tip);
        });

    }, 2000);
}